mirroring_add 1 1
mirroring_add 2 2
table_add is_smtag_present smtag_is_present 1 =>
table_add is_smtag_present no_smtag 0 =>
table_add eth_fwd a_fwd 0x000400000000 => 1
table_add eth_fwd a_fwd 0x000400000001 => 2
table_add eth_fwd broadcast 0xFFFFFFFFFFFF => 2
table_add clone _no_op 3 =>
table_add clone mod_and_clone 2 => 1
table_add clone mod_and_clone 1 => 0
table_add clone _drop 0 =>
table_add e_filter _drop 1 1 =>
table_add e_filter _drop 2 2 =>
table_set_default e_filter _no_op
