#!/usr/bin/python

from scapy.all import sniff, sendp
from scapy.all import Packet
from scapy.all import ShortField, IntField, XLongField, XBitField
from scapy.all import Ether, IP, TCP

import random

import sys

class Vibrant(Packet):
  name = "Vibrant"
  fields_desc = [
    XBitField("flag", 0x020101010101, 48),
    XLongField("hKeyIndex", 0)
  ]

def main():
  hki = 0
  sh = 0
  for i in range(4):
    hki += random.randint(0, 65535) << sh
    sh += 16
  p = Vibrant(hKeyIndex=hki) / Ether() / IP() / TCP() / "AAAA"
  print p.show()
  sendp(p, iface = "eth0")

if __name__ == '__main__':
  main()
