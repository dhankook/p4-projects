#define VIBRANT_FLAG 0x020101010101

header_type vibrant_t {
  fields {
    flag : 48;
    hKeyIndex : 64;
  }
}

header vibrant_t vibrant;

parser start {
  return select(current(0, 48)) {
    VIBRANT_FLAG: parse_vibrant;
    default: ingress;
  }
}

parser parse_vibrant {
  extract(vibrant);
  return ingress;
}

action _no_op() {
}

action a_strip_vibrant() {
  remove_header(vibrant);
}

table strip_vibrant {
  reads {
    vibrant : valid;
  }
  actions {
    a_strip_vibrant;
    _no_op;
  }
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}

control ingress {
    apply(strip_vibrant);
    apply(fwd);
}
