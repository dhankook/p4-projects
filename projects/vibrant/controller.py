#!/usr/bin/python

import random
import argparse
import sys
from cStringIO import StringIO
import runtime_CLI
from sswitch_CLI import SimpleSwitchAPI
import socket
import time

# TODO: eval multiprocessing.dummy
from multiprocessing import Process, Lock, Pool

import itertools

import code
# code.interact(local=dict(globals(), **locals()))


class RtaError(Exception):
  pass

# http://stackoverflow.com/questions/16571150/how-to-capture-stdout-output-from-a-python-function-call
class Capturing(list):
  def __enter__(self):
    self._stdout = sys.stdout
    sys.stdout = self._stringio = StringIO()
    return self
  def __exit__(self, *args):
    self.extend(self._stringio.getvalue().splitlines())
    del self._stringio
    sys.stdout = self._stdout

class Device(object):
  def __init__(self, ip, port):
    self.ip = ip
    self.port = port
    self.lock = Lock()
    services = runtime_CLI.RuntimeAPI.get_thrift_services(runtime_CLI.PreType.SimplePreLAG)
    services.extend(SimpleSwitchAPI.get_thrift_services())
    try:
      std_client, mc_client, sswitch_client = runtime_CLI.thrift_connect(ip, port, services)
    except:
        return "Error - create_device: " + str(sys.exc_info()[0])

    json = 'vibrant.json'
    runtime_CLI.load_json_config(std_client, json)
    self.rta = SimpleSwitchAPI(runtime_CLI.PreType.SimplePreLAG, std_client,
                               mc_client, sswitch_client)

    # key for each of these dicts is epoch
    self.enc_handles = {}
    self.dec_handles = {}

def do_table_modify(device, rule):
  #print ('do_table_modify')
  try:
    device.lock.acquire()
    device.rta.do_table_modify(rule)
    device.lock.release()
  except:
    raise RtaError('table_modify raised an exception (rule: ' + rule + ')')
  return 'do_table_modify'

class Controller(object):

  def __init__(self, args):
    self.h_width = args.hwidth
    self.k_width = args.ewidth
    self.outfile = args.outfile
    self.threaded = args.threaded
    self.device1 = Device('localhost', 22222)
    self.device2 = Device('localhost', 22223)
    self.pool = Pool(processes=2)
    self.counter = 0
    self.keys = {} # {epoch (int) : keys (list of strs)}

  def gen_subkey(self, nbytes):
    ret = '0x'
    for i in range(nbytes):
      ret += hex(random.randint(0, 255)).split('0x')[1]
    return ret

  def do_table_add(self, device, rule):
    with Capturing() as output:
      try:
        device.rta.do_table_add(rule)
      except KeyboardInterrupt:
        pass
      except:
        raise RtaError('table_add raised an exception (rule: ' + rule + ')')
    for out in output:
      # print(out)
      if 'Entry has been added' in out:
        return int(out.split('handle ')[1])
    raise RtaError('table_add: did not detect success (rule: ' + rule + ')')

  def gen_mask_and_keys(self):
    h_width = self.h_width
    k_width = self.k_width
    outfile = self.outfile
    assert(h_width > 0)
    assert(h_width > k_width)

    bits = []
    mask = 0

    for i in range(k_width):
      done = False
      while(done == False):
        bit = random.randint(0, h_width - 1)
        if bit not in bits:
          bits.append(bit)
          done = True
  
    for bit in bits:
      mask = mask | (1 << bit)
    
    with open(outfile, 'w') as f:
      print("MASK: " + hex(mask))
      f.write(hex(mask) + '\n')

      for i in range(2**k_width):
        keybits = [0] * k_width
        value = i
        for j in range(k_width):
          keybits[j] = value & 1
          value = value >> 1
        # print(str(keybits[::-1]))
        epoch = 0
        for j in range(k_width):
          epoch = epoch | (keybits[j] << bits[j])
        rule = 'decrypt a_decrypt ' + hex(epoch) + '&&&' + hex(mask) + ' => '
        k1 = self.gen_subkey(6)
        k2 = self.gen_subkey(6)
        k3 = self.gen_subkey(4)
        k4 = self.gen_subkey(4)
        self.keys[epoch] = [k1, k2, k3, k4]
        rule += k1 + ' ' + k2 + ' ' + k3 + ' ' + k4 + ' 1'
        f.write(rule + '\n')

        handle = self.do_table_add(self.device1, rule)
        self.device1.dec_handles[epoch] = handle

        handle = self.do_table_add(self.device2, rule)
        self.device2.dec_handles[epoch] = handle

        rule = 'encrypt a_encrypt ' + hex(epoch) + '&&&' + hex(mask) + ' => '
        rule += k1 + ' ' + k2 + ' ' + k3 + ' ' + k4 + ' 1'
        f.write(rule + '\n')

        handle = self.do_table_add(self.device1, rule)
        self.device1.enc_handles[epoch] = handle

        handle = self.do_table_add(self.device2, rule)
        self.device2.enc_handles[epoch] = handle

  def update_epoch(self, epoch):
    "rule_mod: \'<table name> <action> <handle> <[aparams]>\'"

    print('Epoch: ' + hex(epoch))
    k1, k2, k3, k4 = self.keys[epoch]
    print('Old Keys: ' + k1 + ' ' + k2 + ' ' + k3 + ' ' + k4)

    #processes = []

    # 1: Change epoch when assigned to new packets

    self.keys[epoch] = 'unsafe'

    rule_mod_part1 = 'encrypt a_mod_epoch_and_encrypt '

    done = False
    while done == False:
      new_epoch = self.keys.keys()[random.randint(0, 2**self.k_width - 1)]
      if self.keys[new_epoch] != 'unsafe':  
        k1, k2, k3, k4 = self.keys[new_epoch]
        done = True

    rule_mod_part2 = ' ' + hex(new_epoch) + ' '
    rule_mod_part2 += k1 + ' ' + k2 + ' ' + k3 + ' ' + k4

    handle = self.device1.enc_handles[epoch]
    print('Dev 1 encrypt handle: ' + str(handle))
    rule_mod1 = rule_mod_part1 + str(handle) + rule_mod_part2
    handle = self.device2.enc_handles[epoch]
    rule_mod2 = rule_mod_part1 + str(handle) + rule_mod_part2

    results = []

    if(self.threaded):
      """
      p = Process(target=self.do_table_modify, args=(self.device1, rule_mod1))
      p.start()
      processes.append(p)
      p = Process(target=self.do_table_modify, args=(self.device1, rule_mod1))
      p.start()
      processes.append(p)
      """
      #self.pool.apply_async(self.do_table_modify, (self.device1, rule_mod1,))
      #self.pool.apply_async(self.do_table_modify, (self.device2, rule_mod2,))

      #results.append(self.pool.apply_async(do_table_modify, (self.device1, rule_mod1)))
      results.append(self.pool.apply_async(do_test, (self.device1, rule_mod1)))
      #results.append(self.pool.apply_async(do_table_modify, (self.device2, rule_mod2)))

    else:
      self.device1.rta.do_table_modify(rule_mod1)
      self.device2.rta.do_table_modify(rule_mod2)

    # 2: Update key
    k1 = self.gen_subkey(6)
    k2 = self.gen_subkey(6)
    k3 = self.gen_subkey(4)
    k4 = self.gen_subkey(4)
    self.keys[epoch] = [k1, k2, k3, k4]

    # 3: Wait for packets in flight w/ old epoch to drain
    # By this point, more than likely, all packets with
    #  the old epoch have already drained.  For now,
    #  we forego any explicit attempt to wait an RTT or to
    #  detect that such packets are still in flight, but
    #  we will evaluate whether epoch updates cause packet loss.

    # 4: Update decrypt keys everywhere
    rule_mod_part1 = 'decrypt a_decrypt '
    rule_mod_part2 = ' ' + k1 + ' ' + k2 + ' ' + k3 + ' ' + k4

    print('New keys:' + rule_mod_part2 + '\n')

    handle = self.device1.dec_handles[epoch]
    rule_mod1 = rule_mod_part1 + str(handle) + rule_mod_part2
    handle = self.device2.dec_handles[epoch]
    rule_mod2 = rule_mod_part1 + str(handle) + rule_mod_part2
    if(self.threaded):
      results.append(self.pool.apply_async(do_table_modify, (self.device1, rule_mod1,)))
      results.append(self.pool.apply_async(do_table_modify, (self.device2, rule_mod2,)))
    else:
      self.device1.rta.do_table_modify(rule_mod1)
      self.device2.rta.do_table_modify(rule_mod2)

    # 5: Stop changing epoch when assigned to new packets 
    rule_mod_part1 = 'encrypt a_encrypt '
    rule_mod_part2 = ' ' + k1 + ' ' + k2 + ' ' + k3 + ' ' + k4

    handle = self.device1.enc_handles[epoch]
    rule_mod1 = rule_mod_part1 + str(handle) + rule_mod_part2
    handle = self.device2.enc_handles[epoch]
    rule_mod2 = rule_mod_part1 + str(handle) + rule_mod_part2

    if(self.threaded):
      results.append(self.pool.apply_async(do_table_modify, (self.device1, rule_mod1,)))
      results.append(self.pool.apply_async(do_table_modify, (self.device2, rule_mod2,)))
      code.interact(local=dict(globals(), **locals()))
      #for res in results:
      #  print res.get()

    else:
      self.device1.rta.do_table_modify(rule_mod1)
      self.device2.rta.do_table_modify(rule_mod2)

    #if(self.threaded):
    #  for p in processes:
    #    p.join()

#def server(args):
#  ctrl = Controller(args)
#  ctrl.serverloop()

def parse_args(args):
  parser = argparse.ArgumentParser(description='VIBRANT AP Control')
  parser.add_argument('--hwidth', help='hidden epoch field width',
                      type=int, action='store', default=64)
  parser.add_argument('--ewidth', help='actual epoch width',
                      type=int, action='store', default=8)
  parser.add_argument('--outfile', help='output file',
                      type=str, action='store', default='entries')
  parser.add_argument('--threaded', help='enable threading',
                      action="store_true")
  #parser.set_defaults(func=server)
  return parser.parse_args(args)

def main():
  args = parse_args(sys.argv[1:])
  #args.func(args)
  ctrl = Controller(args)
  ctrl.gen_mask_and_keys()

  raw_input("Press Enter to begin key rotation...")

  count = 0
  start = time.time()
  while count < 1:
    for i in range(2**args.ewidth):
      ctrl.update_epoch(ctrl.keys.keys()[i])
    count += 1
  end = time.time()
  print(end - start)
  print('Controller counter: ' + str(ctrl.counter))

if __name__ == '__main__':
  main()
