#!/usr/bin/python

from scapy.all import sniff, sendp
from scapy.all import Packet
from scapy.all import ShortField, IntField, LongField, BitField

import sys
import struct

import code
# code.interact(local=dict(globals(), **locals()))

def printhex(s):
  return '0x' + "".join("{:02x}".format(ord(c)) for c in s)

def handle_pkt(pkt):
  pkt = str(pkt)
  if len(pkt) < 12: return
  flag = pkt[:6]
  vib_flag = "\x02\x01\x01\x01\x01\x01"
  if flag != vib_flag: return
  hKeyIndex = struct.unpack("!Q", pkt[6:14])[0]
  print("hKeyIndex: " + hex(hKeyIndex))
  ethDst = pkt[14:20]
  ethSrc = pkt[20:26]
  ethType = pkt[26:28]
  print("ethDst: " + printhex(ethDst))
  print("ethSrc: " + printhex(ethSrc))
  print("ethType: " + printhex(ethType))
  if ethType == "\x08\x00":
    ipSrc = pkt[40:44]
    ipDst = pkt[44:48]
    print("ipSrc: " + printhex(ipSrc))
    print("ipDst: " + printhex(ipDst))
  sys.stdout.flush()

def main():
  sniff(iface = "eth0",
        prn = lambda x: handle_pkt(x))

if __name__ == '__main__':
  main()
