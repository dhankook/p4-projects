header_type ext_t {
  fields {
    data : 8;
  }
}

header ext_t ext[100];

parser start {
  extract(ext[0]);
  return ingress;
}

action a_test() {
  push(ext, 1);
}

table test {
  actions {
    a_test;
  }
}

action a_fwd(egress_spec) {
  modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_fwd;
  }
}

control ingress {
  apply(test);
  apply(fwd);
}
