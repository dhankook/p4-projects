header_type ext_t {
  fields {
    data : 8;
  }
}

/*
header_type meta_t {
  fields {
    data : 8;
  }
}
*/

header ext_t ext[100];
//metadata meta_t meta;

parser start {
  extract(ext[0]);
  return ingress;
}

/*
action a_setup() {
  modify_field(meta.data, ext[0].data);
}

table setup {
  actions {
    a_setup;
  }
}

action a_test1() {
  pop(ext, 1);
}

action _no_op() {
}

table test1 {
  actions {
    a_test1;
    _no_op;
  }
}
*/

action a_test2() {
  push(ext, 1);
}

table test2 {
  actions {
    a_test2;
  }
}

action a_fwd(egress_spec) {
  modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_fwd;
  }
}

/*
action a_wrapup() {
  modify_field(ext[0].data, meta.data);
}

table wrapup {
  actions {
    a_wrapup;
  }
}
*/

control ingress {
  //apply(setup);
  //apply(test1);
  apply(test2);
  apply(fwd);
  //apply(wrapup);
}
