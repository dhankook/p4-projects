header_type ethernet_t {
  fields {
    dstAddr : 48;
    srcAddr : 48;
    etherType : 16;
  }
}

header_type meta_t {
  fields {
    val : 32;
  }
}

header ethernet_t ethernet;
metadata meta_t meta;

parser start {
  extract(ethernet);
  return ingress;
}

// rightshift and tmask set to 9 bits b/c that is the width of sm.egress_spec
/*
action mod_stdmeta_egressspec_meta(rightshift, tmask) {
  modify_field(standard_metadata.egress_spec, (tmeta.data >> rightshift) & tmask);
}
*/

action bw_test(rightshift) {
  modify_field(standard_metadata.egress_spec, (meta.val >> rightshift));
  modify_field(ethernet.etherType, standard_metadata.egress_spec);
}

action val_init(val) {
  modify_field(meta.val, val);
}

table init {
  actions {
    val_init;
  }
}

table test {
  actions {
    bw_test;
  }
}

action a_fwd(port) {
  modify_field(standard_metadata.egress_spec, port);
}

table fwd {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_fwd;
  }
}

control ingress {
  apply(init);
  apply(test);
  apply(fwd);
}
