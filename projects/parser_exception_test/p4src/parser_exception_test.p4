header_type big_t {
  fields {
    data : 512;
  }
}

header_type small_t {
  fields {
    data : 32;
  }
}

header_type err_t {
  fields {
    toobig : 8;
  }
}

header big_t big;
header small_t small;
metadata err_t err;

parser start {
  extract(big);
  parse_error p4_pe_out_of_packet;
}

parser_exception p4_pe_out_of_packet {
  set_metadata(err.toobig, 1);
  return ingress;
}

action _no_op() {
  no_op();
}

action _drop() {
  drop();
}

action nottoobig() {
}

action toobig() {
}

table check_err {
  reads {
    err.toobig : exact;
  }
  actions {
    nottoobig;
    toobig;
  }
}

action a_set_egress(port) {
  modify_field(standard_metadata.egress_spec, port);
}

table set_egress {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_set_egress;
  }
}

control ingress {
  apply(check_err);
  apply(set_egress);
}
