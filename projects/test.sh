#!/bin/bash

#find ./ -maxdepth 2 -mindepth 2 -type f -name "run.sh" \
#        -not -path "./hobfs/*" \
#        -not -path "./arp_proxy/*" \
#        -exec /bin/rm {} \;

find ./ -maxdepth 1 -mindepth 1 -type d \
        -not -path "./hobfs" \
        -not -path "./arp_proxy" \
        -exec /bin/cp ./arp_proxy/run.sh {} \;

find ./ -maxdepth 1 -mindepth 1 -type d \
        -not -path "./hobfs" \
        -not -path "./arp_proxy" \
        -exec /bin/cp ./arp_proxy/run_cli.sh {} \;

find ./ -maxdepth 1 -mindepth 1 -type d \
        -not -path "./hobfs" \
        -not -path "./arp_proxy" \
        -exec /bin/cp ./arp_proxy/run_nano.sh {} \;
