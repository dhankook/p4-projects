header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
}

header ethernet_t ethernet;
header ipv4_t ipv4;

field_list ipv4_checksum_list {
        ipv4.version;
        ipv4.ihl;
        ipv4.diffserv;
        ipv4.totalLen;
        ipv4.identification;
        ipv4.flags;
        ipv4.fragOffset;
        ipv4.ttl;
        ipv4.protocol;
        ipv4.srcAddr;
        ipv4.dstAddr;
}

field_list_calculation ipv4_checksum {
    input {
        ipv4_checksum_list;
    }
    algorithm : csum16;
    output_width : 16;
}

calculated_field ipv4.hdrChecksum  {
    update ipv4_checksum if (valid(ipv4));
}

parser start {
  extract(ethernet);
  return select(ethernet.etherType) {
    0x0800 : parse_ipv4;
    default : ingress;
  }
}

parser parse_ipv4 {
  extract(ipv4);
  return ingress;
}

action a_ttl_edit() {
  modify_field(ipv4.ttl, 42);
}

action _no_op() {
}

table ttl_edit {
  reads {
    ipv4 : valid;
  }
  actions {
    a_ttl_edit;
    _no_op;
  }
}

action a_fwd(port) {
  modify_field(standard_metadata.egress_spec, port);
}

table eth_fwd {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_fwd;
  }
}

table ipv4_fwd {
  reads {
    ipv4.dstAddr : exact;
  }
  actions {
    a_fwd;
  }
}

control ingress {
  apply(eth_fwd);
  apply(ttl_edit);
  apply(ipv4_fwd);
}
