header_type ethernet_t {
  fields {
    dst : 48;
    src : 48;
    ethtype : 16;
  }
}

header_type meta_t {
  fields {
    instance_ID : 8;
    virt_ingress_port : 8;
    virt_egress_spec : 8;
    virt_fwd_flag : 8;
    egress_override : 8;
    temp_egress_override : 8;
  }
}

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
    }
}

metadata meta_t meta;
metadata intrinsic_metadata_t intrinsic_metadata;
header ethernet_t ethernet;

parser start {
  extract(ethernet);
  return ingress;
}

action a_set_context(ID, vingress) {
  modify_field(meta.instance_ID, ID);
  modify_field(meta.virt_ingress_port, vingress);
}

table tset_context {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_set_context;
  }
}

action set_eth_type(t) {
  modify_field(ethernet.ethtype, t);
}

action add_eth_type(t) {
  modify_field(ethernet.ethtype, ethernet.ethtype + t);
}

action mult_eth_type(t) {
  modify_field(ethernet.ethtype, ethernet.ethtype * t);
}

action _no_op() {
}

table func {
  reads {
    meta.instance_ID : exact;
  }
  actions {
    set_eth_type;
    add_eth_type;
    mult_eth_type;
    _no_op;
  }
}

action a_set_egress(egr_spec) {
  modify_field(meta.virt_egress_spec, egr_spec);
}

table set_egress {
  reads {
    meta.instance_ID : exact;
    meta.virt_ingress_port : exact;
  }
  actions {
    a_set_egress;
  }
}

action _drop() {
  drop();
}

action no_virt(spec) {
  modify_field(standard_metadata.egress_spec, spec);
}

action no_virt_mcast(mcast_grp) {
  modify_field(intrinsic_metadata.mcast_grp, mcast_grp);
}

action yes_virt() {
  modify_field(standard_metadata.egress_spec, standard_metadata.ingress_port);
  modify_field(meta.virt_fwd_flag, 1);
}

table t_virtnet {
  reads {
    meta.instance_ID : exact;
    meta.virt_egress_spec : exact;
  }
  actions {
    _drop;
    no_virt;
    no_virt_mcast;
    yes_virt;
  }
}

control ingress {
  if(meta.instance_ID == 0) {
    apply(tset_context);
  }
  apply(func);
  apply(set_egress);
  apply(t_virtnet);
}

table egress_filter { actions { _drop; } }

field_list fl_recirc {
  standard_metadata;
  meta;
}

field_list fl_clone {
  standard_metadata;
  meta;
}

action vfwd(inst_ID, vingress) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  recirculate(fl_recirc);
}

action vmcast(inst_ID, vingress, vegress) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  modify_field(meta.virt_egress_spec, vegress);
  recirculate(fl_recirc);
  clone_egress_pkt_to_egress(standard_metadata.egress_port, fl_clone);
}

action vmcast_phys(inst_ID, vingress, vegress, phys_spec) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  modify_field(meta.virt_egress_spec, vegress);
  recirculate(fl_recirc);
  clone_egress_pkt_to_egress(phys_spec, fl_clone);
}

action pmcast(vegress, phys_spec) {
  modify_field(meta.virt_egress_spec, vegress);
  clone_egress_pkt_to_egress(phys_spec, fl_clone);
}

table t_egr_virtnet {
  reads {
    meta.instance_ID : exact;
    meta.virt_egress_spec : exact;
  }
  actions {
    vfwd;
    vmcast;
    vmcast_phys;
    pmcast;
    _drop;
  }
}

control egress {
  if(meta.virt_fwd_flag == 1) {
    apply(t_egr_virtnet); // recirculate, maybe clone_e2e
  }
  else if(standard_metadata.egress_port == standard_metadata.ingress_port) {
    apply(egress_filter);
  }
}
