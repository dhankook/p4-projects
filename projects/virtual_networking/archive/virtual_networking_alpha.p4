// Virtual networking demo
// See commands_t* and virt_topo_t* files
// Invoke: ./run_demo_bmv2.sh [one of the commands_t* files]
// TODO: implement fully mixed multicasting.  For bmv2, need to
//       set intrinsic_metadata.mcast_grp in *ingress* pipeline.
//       Need to work out coordination for correct processing
//       in egress pipeline.

header_type ethernet_t {
  fields {
    dst : 48;
    src : 48;
    ethtype : 16;
  }
}

header_type meta_t {
  fields {
    instance_ID : 8;
    virt_ingress_port : 8;
    virt_egress_spec : 8;
    virt_fwd_flag : 8;
  }
}

header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
    }
}

metadata meta_t meta;
metadata intrinsic_metadata_t intrinsic_metadata;
header ethernet_t ethernet;

parser start {
  extract(ethernet);
  return ingress;
}

action a_set_context(ID, vingress) {
  modify_field(meta.instance_ID, ID);
  modify_field(meta.virt_ingress_port, vingress);
}

table tset_context {
  reads {
    standard_metadata.ingress_port : exact;
  }
  actions {
    a_set_context;
  }
}

action set_eth_type(t) {
  modify_field(ethernet.ethtype, t);
}

action add_eth_type(t) {
  modify_field(ethernet.ethtype, ethernet.ethtype + t);
}

action mult_eth_type(t) {
  modify_field(ethernet.ethtype, ethernet.ethtype * t);
}

action _no_op() {
}

table func {
  reads {
    meta.instance_ID : exact;
  }
  actions {
    set_eth_type;
    add_eth_type;
    mult_eth_type;
    _no_op;
  }
}

action a_set_egress(egr_spec) {
  modify_field(meta.virt_egress_spec, egr_spec);
}

table set_egress {
  reads {
    meta.instance_ID : exact;
    meta.virt_ingress_port : exact;
  }
  actions {
    a_set_egress;
  }
}

action _drop() {
  drop();
}

action phys_fwd(port) {
  modify_field(standard_metadata.egress_spec, port);
  modify_field(meta.virt_egress_spec, 0);
}

action virt_fwd() {
  modify_field(standard_metadata.egress_spec, standard_metadata.ingress_port);
  modify_field(meta.virt_fwd_flag, 1);
}

action virt_mcast() {
  modify_field(standard_metadata.egress_spec, standard_metadata.ingress_port);
  modify_field(meta.virt_fwd_flag, 1);
}

action phys_mcast(grp) {
  modify_field(intrinsic_metadata.mcast_grp, grp);
  modify_field(meta.virt_egress_spec, 0);
}

table t_virtnet {
  reads {
    meta.instance_ID : exact;
    meta.virt_egress_spec : exact;
  }
  actions {
    phys_fwd;
    virt_fwd;
    phys_mcast;
    virt_mcast;
    _drop;
  }
}

control ingress {
  if(meta.instance_ID == 0) {
    apply(tset_context);
  }
  apply(func);
  apply(set_egress);
  apply(t_virtnet);
}

field_list fl_clone {
  standard_metadata;
  meta;
}

field_list fl_recirc {
  standard_metadata;
  meta;
}

table egress_filter_1 { 
  actions { 
    _drop; 
  }
}

table egress_filter_2 { actions { _drop; } }

action egr_virt_fwd(inst_ID, vingress) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  modify_field(meta.virt_egress_spec, 0);
  recirculate(fl_recirc);
}

action egr_virt_mcast(inst_ID, vingress, vegress_next) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  modify_field(meta.virt_egress_spec, vegress_next);
  recirculate(fl_recirc);
  clone_egress_pkt_to_egress(standard_metadata.egress_port, fl_clone);
}

action egr_mixed_mcast(inst_ID, vingress, vegress_next, ph_egress_spec) {
  modify_field(meta.instance_ID, inst_ID);
  modify_field(meta.virt_ingress_port, vingress);
  modify_field(meta.virt_egress_spec, vegress_next);
  recirculate(fl_recirc);
  clone_egress_pkt_to_egress(ph_egress_spec, fl_clone);
}

table t_out_virtnet {
  reads {
    meta.instance_ID : exact;
    meta.virt_egress_spec : exact;
  }
  actions {
    egr_virt_fwd;
    egr_virt_mcast;
    egr_mixed_mcast;
    _drop;
    _no_op;
  }
}

control egress {
  if(standard_metadata.egress_port == standard_metadata.ingress_port) {
    if(meta.virt_fwd_flag == 0) {
      apply(egress_filter_1); // physical egress filter
    }
    else if(meta.virt_egress_spec != 0) {
      if(meta.virt_egress_spec == meta.virt_ingress_port) {
        apply(egress_filter_2); // virtual egress filter
      }
      else {
        apply(t_out_virtnet);
      }
    }
  }
}
