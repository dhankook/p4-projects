mirroring_add 1 1
mirroring_add 2 2
table_add tset_context a_set_context 1 => 1 1
table_add tset_context a_set_context 2 => 1 2
table_add func _no_op 1 =>
table_add func set_eth_type 2 => 0x2222
table_add func set_eth_type 3 => 0x3333
table_add set_egress a_set_egress 1 1 => 70
table_add set_egress a_set_egress 1 2 => 70
table_add set_egress a_set_egress 2 2 => 1
table_add set_egress a_set_egress 3 2 => 1
table_add t_virtnet virt_mcast 1 70 =>
table_add t_virtnet phys_fwd 2 1 => 2
table_add t_virtnet phys_fwd 3 1 => 2
table_set_default t_virtnet _drop
table_add t_out_virtnet egr_virt_mcast 1 70 => 2 2 71
table_add t_out_virtnet egr_virt_mcast 2 71 => 3 2 72
table_add t_out_virtnet _drop 3 72 =>
table_set_default egress_filter_1 _drop
table_set_default egress_filter_2 _drop
