mirroring_add 1 1
mirroring_add 2 2
mirroring_add 3 3
mirroring_add 4 4
table_add tset_context a_set_context 1 => 1 1
table_add tset_context a_set_context 2 => 1 2
table_add tset_context a_set_context 3 => 2 1
table_add tset_context a_set_context 4 => 2 2
table_add func add_eth_type 1 => 1
table_add func mult_eth_type 2 => 2
table_add func add_eth_type 3 => 3
table_add set_egress a_set_egress 1 1 => 70
table_add set_egress a_set_egress 1 2 => 4
table_add set_egress a_set_egress 1 3 => 1
table_add set_egress a_set_egress 1 4 => 2
table_add set_egress a_set_egress 2 1 => 3
table_add set_egress a_set_egress 2 2 => 4
table_add set_egress a_set_egress 2 3 => 1
table_add set_egress a_set_egress 2 4 => 2
table_add set_egress a_set_egress 3 1 => 2
table_add set_egress a_set_egress 3 2 => 1
table_add t_virtnet no_virt 1 1 => 1
table_add t_virtnet no_virt 1 2 => 2
table_add t_virtnet yes_virt 1 70 =>
table_add t_virtnet yes_virt 1 4 =>
table_add t_virtnet no_virt 2 1 => 3
table_add t_virtnet no_virt 2 2 => 4
table_add t_virtnet yes_virt 2 3 =>
table_add t_virtnet yes_virt 2 4 =>
table_add t_virtnet yes_virt 3 1 =>
table_add t_virtnet yes_virt 3 2 =>
table_set_default t_virtnet _drop
table_add t_egr_virtnet vmcast 1 70 => 2 3
table_add t_egr_virtnet vfwd 1 71 => 3 1
table_add t_egr_virtnet vfwd 1 4 => 3 1
table_add t_egr_virtnet vfwd 2 3 => 1 3
table_add t_egr_virtnet vfwd 2 4 => 3 2
table_add t_egr_virtnet vfwd 3 1 => 1 4
table_add t_egr_virtnet vfwd 3 2 => 2 4
table_set_default egress_filter _drop
table_set_default update_instance_ID a_update_instance_ID
