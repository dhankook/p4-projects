header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        all : 160;
    }
}

/*
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
*/

header ethernet_t ethernet;
header ipv4_t ipv4;

parser start {
  extract(ethernet);
  return select(ethernet.etherType) {
    0x0800 : parse_ipv4;
    default : ingress;
  }
}

parser parse_ipv4 {
  extract(ipv4);
  return ingress;
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}

action math_on_TTL(val, leftshift) {
  modify_field(ipv4.all, ipv4.all + (val << leftshift) );
}

action _no_op() {
}

table math {
  reads {
    ipv4 : valid;
  }
  actions {
    math_on_TTL;
    _no_op;
  }
}

control ingress {
  apply(fwd);
  apply(math);
}
