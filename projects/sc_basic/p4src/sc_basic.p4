#define TR_A 0x7777
#define TR_B 0x7778
#define IPV4 0x0800

#define T_INT   0x0000
#define T_ASCII 0x0001
#define T_PROTO 0xFFFE
#define T_ERROR 0xFFFF

header_type test_report_A_t {
  fields {
    type1: 16;
    val1:  16;
    type2: 16;
    val2:  16;
  }
}

header_type test_report_B_t {
  fields {
    type1: 16;
    val1:  16;
    type2: 16;
    val2:  16;
  }
}

header_type ether_t {
    fields {
        dst : 48;
        src : 48;
        e_type : 16;
    }
}

header test_report_A_t test_report_A;
header test_report_B_t test_report_B;
header ether_t ether;

parser start {
  extract(ether);
  return select(ether.e_type) {
    TR_A: parse_tr_A;
    TR_B: parse_tr_B;
    default: ingress;
  }
}

parser parse_tr_A {
  extract(test_report_A);
  return ingress;
}

parser parse_tr_B {
  extract(test_report_B);
  return ingress;
}

action mark_tr_A() {
  modify_field(test_report_A.type1, T_INT);
  modify_field(test_report_A.val1, 0xAAAA);
}

action _no_op() {
}

table check_tr_A {
  reads {
    test_report_A : valid;
  }
  actions {
    mark_tr_A;
    _no_op;
  }
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}

control ingress {
  apply(check_tr_A);
  apply(fwd);
}
