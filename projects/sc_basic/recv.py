#!/usr/bin/python

from scapy.all import sniff, sendp
from scapy.all import Packet
from scapy.all import ShortField, IntField, LongField, BitField

import sys
import struct

IPV4 = "\x08\x00"
TR_A = "\x77\x77"
TR_B = "\x77\x78"

VALUES = {0x0000: "T_INT",
          0x0001: "T_ASCII",
          0xFFFE: "T_PROTO",
          0xFFFF: "T_ERROR"}

def printhex(s):
  return '0x' + "".join("{:02x}".format(ord(c)) for c in s)

def processEthernet(pkt):
  ethDst = pkt[:6]
  ethSrc = pkt[6:12]
  ethType = pkt[12:14]
  print("ethDst: " + printhex(ethDst))
  print("ethSrc: " + printhex(ethSrc))
  print("ethType: " + printhex(ethType))
  return ethType

def handle_pkt(pkt):
  pkt = str(pkt)
  if len(pkt) < 14:
    return
  ethType = processEthernet(pkt)

  if ethType == IPV4:
    ipSrc = pkt[40:44]
    ipDst = pkt[44:48]
    print("ipSrc: " + printhex(ipSrc))
    print("ipDst: " + printhex(ipDst))

  elif ethType == TR_A or ethType == TR_B:
    tr_type1 = struct.unpack("!H", pkt[14:16])[0]
    tr_val1 = struct.unpack("!H", pkt[16:18])[0]
    print("tr_type1: " + VALUES[tr_type1])
    print("tr_val1:  " + hex(tr_val1))

  sys.stdout.flush()

def main():
  sniff(iface = "eth0",
        prn = lambda x: handle_pkt(x))

if __name__ == '__main__':
  main()
