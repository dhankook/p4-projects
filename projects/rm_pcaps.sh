#!/bin/bash

find ./ -maxdepth 2 -mindepth 2 -type f -name "*.pcap" \
        -exec /bin/rm {} \;
