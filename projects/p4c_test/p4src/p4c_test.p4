#define PKT_INSTANCE_TYPE_NORMAL 0
#define PKT_INSTANCE_TYPE_INGRESS_CLONE 1
#define PKT_INSTANCE_TYPE_EGRESS_CLONE 2
#define PKT_INSTANCE_TYPE_COALESCED 3
#define PKT_INSTANCE_TYPE_INGRESS_RECIRC 4
#define PKT_INSTANCE_TYPE_REPLICATION 5
#define PKT_INSTANCE_TYPE_RESUBMIT 6

header_type ethernet_t {
  fields {
    byte1 : 8;
    byte2 : 8;
    byte3 : 8;
    byte4 : 8;
  }
}

header_type meta_t {
  fields {
    meta1 : 8;
    meta2 : 8;
    meta3 : 8;
    meta4 : 8;
  }
}

//  bmv2 simple switch requires this intrinsic metadata structure
header_type intrinsic_metadata_t {
  fields {
        ingress_global_timestamp : 48;
        egress_global_timestamp : 48;
        mcast_grp : 16;
        egress_rid : 16;
  }
}

header ethernet_t ethernet;
metadata meta_t meta;
metadata intrinsic_metadata_t intrinsic_metadata;

parser start {
  extract(ethernet);
  return ingress;
}

action set_egr(egress_spec) {
    modify_field(standard_metadata.egress_spec, egress_spec);
    modify_field(meta.meta1, 0x55);
}

table fwd {
    reads {
        standard_metadata.ingress_port : exact;
    }
    actions {
        set_egr;
    }
}

control ingress {
    apply(fwd);
}

action _no_op() {
}

table t_check_egress {
  reads {
    standard_metadata.egress_spec : exact;
  }
  actions {
    _no_op;
  }
}

field_list fl_clone {
  standard_metadata;
  meta;
}

action a_clone() {
  clone_egress_pkt_to_egress(standard_metadata.egress_port, fl_clone);
}

action a_handle_clone() {
  modify_field(ethernet.byte1, meta.meta1);
}

table t_clone {
  reads {
    standard_metadata.instance_type : exact;
  }
  actions {
    a_clone;
    a_handle_clone;
  }
}

control egress {
  apply(t_check_egress);
  apply(t_clone);
}
