#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# set MININET_PATH, BMV2_PATH, P4C_BM_PATH
source $THIS_DIR/../env.sh

#source $THIS_DIR/../run_demo.sh

PROJ=${PWD##*/}

P4C_BM_SCRIPT=$P4C_BM_PATH/p4c_bm/__main__.py

SWITCH_PATH=$BMV2_PATH/targets/simple_switch/simple_switch

CLI_PATH=$BMV2_PATH/targets/simple_switch/sswitch_CLI

PRE=""
SCENARIO=""
SEED=""
COMMANDS=""
TOPO=""

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    --pre)
    PRE="--pre $2"
    shift # past argument
    ;;
    --scenario)
    SCENARIO="--scenario $2"
    shift # past argument
    ;;
    --seed)
    SEED="--seed $2"
    shift # past argument
    ;;
    -c|--commands)
    COMMANDS="$COMMANDS $2"
    shift # past argument
    ;;
    -t|--topo)
    TOPO="--topo $2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

if [ ! -z "${COMMANDS}" ]; then
  COMMANDS="--commands "$COMMANDS
fi

#$P4C_BM_SCRIPT p4src/$PROJ.p4 --json $PROJ.json
p4c --target bmv2 --arch v1model --std p4-14 p4src/p4c_test.p4
sudo PYTHONPATH=$PYTHONPATH:$BMV2_PATH/mininet/ \
    python $MININET_PATH/topo.py \
    --behavioral-exe $SWITCH_PATH \
    --json $PROJ.json \
    $COMMANDS \
    --cli $CLI_PATH \
    $SCENARIO \
    $SEED \
    $TOPO \
    $PRE
