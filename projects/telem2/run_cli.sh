#!/bin/bash

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $THIS_DIR/../env.sh

PROJ=${PWD##*/}

ARG1=22222

if [ "$#" -gt 0 ]; then
  ARG1=$1
fi

CLI_PATH=$BMV2_PATH/targets/simple_switch/sswitch_CLI

$CLI_PATH $PROJ.json $ARG1
