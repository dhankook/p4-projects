parser start {
  extract(ethernet);
  return select(ethernet.type) {
    0x0800 : ipv4;
    // ...
  }
}

parser udp {
  extract(udp);
  return select(udp.src) {
    53 : parse_dns;
    default : ingress;
  }
}

parser parse_dns {
  extract(dns_begin);
  return select(udp.len) {
    42 : dns_42;
    43 : dns_43;
    44 : dns_44;
    //
  }
}

parser dns_42 {
  extract(dns_42);
  return ingress;
}

action merge() {
  modify_field(meta.domain, dns_42.data);
  modify_field(meta.domain, dns_43.data);
  modify_field(meta.domain, dns_44.data);
}

table t_merge {
  actions {
    merge;
  }
}

table do_things {
  reads {
    meta.domain : exact;
  }
  actions {
  }
}

control ingress {
  apply(t_merge);
  apply(do_things);
}
