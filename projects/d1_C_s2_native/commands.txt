table_add init_meta_egress a_init_meta_egress 1 => 2
table_add init_meta_egress a_init_meta_egress 2 => 1
table_add check_arp arp_present 1 =>
table_add check_arp send_packet 0 =>
table_add check_opcode arp_request 1 =>
table_set_default check_opcode send_packet
table_add handle_arp_request arp_reply 0x0a00010a => 0x000400000001
table_add handle_arp_request arp_reply 0x0a00000a => 0x000400000000
table_set_default handle_arp_request send_packet
table_add is_tcp_or_udp_valid tcp_present 1 0 =>
table_add is_tcp_or_udp_valid udp_present 0 1 =>
table_set_default is_tcp_or_udp_valid _no_op
table_add tcp_block _drop 4000&&&0xFFFF 0&&&0x00 => 1
table_add udp_block _drop 0&&&0x00 5000&&&0xFFFF => 1
table_add send_frame rewrite_mac 1 => 0x00aabb000001
table_add send_frame rewrite_mac 2 => 0x00aabb000002
table_add forward set_dmac 0x0A00000A => 0x000400000000
table_add forward set_dmac 0x0A00010A => 0x000400000001
table_add ipv4_lpm set_nhop 0x0A00000A/32 => 0x0A00000A 1
table_add ipv4_lpm set_nhop 0x0A00010A/32 => 0x0A00010A 2
table_set_default csum a_csum
