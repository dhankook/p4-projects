#!/bin/bash

BMV2_PATH=/home/ubuntu/bmv2
SSWITCH_PATH=$BMV2_PATH/targets/simple_switch
PASSTHROUGH_PATH=/home/ubuntu/p4-projects/projects/passthrough

sudo PYTHONPATH=$PYTHONPATH:$BMV2_PATH/mininet/ \
    python -u ./scapy_mininet_example.py \
    --behavioral-exe $SSWITCH_PATH/simple_switch \
    --json $PASSTHROUGH_PATH/passthrough.json \
    --commands $PASSTHROUGH_PATH/commands.txt \
    --cli $SSWITCH_PATH/sswitch_CLI
