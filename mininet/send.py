#!/usr/bin/python

from scapy.all import sniff, sendp
from scapy.all import Packet
from scapy.all import ShortField, IntField, XLongField, XBitField
from scapy.all import Ether, IP, TCP

import random

import sys

TR_A = 0x7777
TR_B = 0x7778

class Test_Report(Packet):
  name = "Test_Report"
  fields_desc = [
    XBitField("type1", 0, 16),
    XBitField("val1", 0, 16),
    XBitField("type2", 0, 16),
    XBitField("val2", 0, 16)
  ]

def main():
  p = Ether(dst="00:04:00:00:00:01", type=TR_A) / Test_Report() / IP() / TCP() / "AAAA"
  print p.show()
  sendp(p, iface = "eth0")

if __name__ == '__main__':
  main()
